package br.com.rootcom.aulaforum.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.rootcom.aulaforum.modelo.Topico;

public interface TopicoRepository extends JpaRepository<Topico, Long> {

	List<Topico> findByCursoNome(String nomeCurso);
	
	

}
