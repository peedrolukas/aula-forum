package br.com.rootcom.aulaforum;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AulaForumApplication {

	public static void main(String[] args) {
		SpringApplication.run(AulaForumApplication.class, args);
	}

}
